Prerequisiti:
- node js installato
- lanciare il comando "npm install"
- avere un container "gearman" in esecuzione

Per lanciare l'applicazione lanciare il seguente comando da terminale:
```
node app.js
```

Per autenticarsi all'applicazione è necessario inviare tramite POST all'url http://localhost:3000/auth il seguente JSON:
```
{ 
	"username":"john",
	"password":"doe"
}
```

Dopo essersi autenticati si otterrà un token JWT, questo dovrà essere usato per autenticarsi per ogni successiva richiesta HTTP.

Per indicare l'endpoint da cui si vogliono ricevere le stringhe codificate in base64 è necessario inviare una POST all' url http://localhost:3000/webhook strutturato nel seguente modo:
```
{ 
	"url":"foo",
}
```

Il passo successivo è quello di inviare una stringa da codificare all'URL http://localhost:3000/encode che si aspetta un parametro denominato "string", che potrai valorizzare con qualsiasi stringa.

Per ottenere la stringa codificata in base64 è necessario collegarsi in POST all'URL avente l'endpoint inviato al server in precedenza (nel nostro esempio http://localhost:3000/foo).

