const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
var url = require('url');
const app = express();
var Gearman = require("node-gearman");

var gearman = new Gearman("localhost",4730);

gearman.registerWorker("encode", function(payload, worker){
	console.log(payload);
    if(!payload){
        worker.error();
        return;
    }
    var encoded = Buffer.from(payload).toString('base64')
    worker.end(encoded);
});
app.use(bodyParser.json());

//access token, don't share it :)
const accessTokenSecret = 'AV5jCeNZIbSdlQJC4fVd';

//middleware for authentication
const authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }

            req.user = user;
            next();
        });
    } else {
        res.sendStatus(401);
    }
};
//array con tutti gli endpoint
var endpoint = [];
//variabile con valore encoded
var encoded = "";
//accepted users (I know that a noSQL DB is the best solution, but for this example I use this way)
const users = [
    {
        username: 'john',
        password: 'doe',
        role: 'admin'
    }
];
//auth endpoint
app.post("/auth",(req,res)=>{
	const { username, password } = req.body;
	const user = users.find(u => { return u.username === username && u.password === password });
	console.log(req.body);
	//if user exists
	if (user) {
        // Generate an access token
        const accessToken = jwt.sign({ username: user.username, password: user.password }, accessTokenSecret);
		//send token
        res.json({
            accessToken
        });
    } 
	else {
        res.send('Username or password incorrect');
    }
})
//webhook
app.post("/webhook",authenticateJWT, (req,res)=>{
	//add endpoint to list
	endpoint.push(req.body.url);
	//send feedback to user
	res.send("Your endpoint is saved");
});
//encode data from params
app.post("/encode",authenticateJWT,(req,res)=>{
	var job = gearman.submitJob("encode", req.query.string);
	job.on("data", function(data){
		encoded = data.toString("utf-8")
		res.send("Your string has been encoded. You can find it in your saved webhook");
	});
	job.on("error", function(error){
		res.send(error.message);
	});
	job.on("end", function(){
		gearman.close();
	});
});
//newly registered endpoints
app.post('/:id', function(req, res) {
	//if endpoint is registered
	if(endpoint.includes(req.params.id)){
		res.send(encoded);
	}
	//else send error message
    else{
		res.send("url non valido")
	}
});

//express is listening on port 3000
app.listen(3000, () => {
    console.log('auth service started on port 3000');
});
